(function ($, Drupal, once) {
    Drupal.behaviors.datatables_filter = {
        attach: function (context, settings) {
            once('datatables_filter', '.datatables-enabled', context).forEach(
                function (thisElement) {
                    // Initialize to an empty object
                    var thisSettings = {};

                    // Check if the attribute exists
                    if (thisElement.hasAttribute("data-datatables-filter-settings")) {
                        // Parse the JSON data attribute
                        thisSettings = JSON.parse(thisElement.getAttribute("data-datatables-filter-settings"));
                    }


                    // Initialize dataTable
                    // Here, replace with the actual DataTables vanilla JS initialization method, if available.
                    // For now, I am assuming it would look something like this:
                    if (typeof DataTable === "function") {
                        new DataTable(thisElement, thisSettings);
                    }
                    else if ((typeof $ !== 'undefined')  && (typeof $.fn.dataTable === "function")) { // If DataTables still requires jQuery
                            $(thisElement).DataTable(thisSettings);
                    } else {
                        console.warn('DataTable is not defined or Jquery missing.');
                    }
                }
            );
        }
    };
})(jQuery, Drupal, once);