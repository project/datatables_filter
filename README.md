# Datatables Filter

Integrates datatables javascript library into a Drupal 8 rich-text filter.

## Requirements
* Drupal 8.x

## Installation
1. Download the datatables library and place it to  `DRUPAL_ROOT/libraries/datatables/`
2. Install the module per normal 
https://www.drupal.org/documentation/install/modules-themes/modules-8.
3. Go to the 'Text formats and editors' configuration page: 
`/admin/config/content/formats`, and for each text format/editor/, enable the filter *Datatables filter*.
4. If "Limit allowed HTML tags and correct faulty HTML" is enabled, make sure 
the `<table class>` tag and other table related HTML tags are included in "Allowed HTML tags" with attributes.

#### Support
Please use the issue queue for filing bugs with this module at
https://www.drupal.org/project/issues/datatables_filter
 

