<?php

namespace Drupal\datatables_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * A filter that adds bootstrap table classes to tables
 *
 * @Filter(
 *   id = "datatables_filter",
 *   title = @Translation("Enable datatables table"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *       "paging" = FALSE,
 *     "searching" = FALSE,
 *     "info" = FALSE,
 *   },
 *   weight = -10
 * )
 */
class DatatablesTable extends FilterBase
{
    protected function getDatatablesOptions()
    {
        $default_cfg = [
            "paging" => [
                "type" => "bool",
                "title" => "Enable or disable table pagination."
                ],
            "searching" => [
                "type" => "bool",
                "title" => "Feature control search (filtering) abilities"
            ],
            "info" => [
              "type" => "bool",
              "title" => "Feature control table information display field"
            ]
        ];
        return $default_cfg;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $opts = $this->getDatatablesOptions();
        foreach ($opts as $opt_key => $opt_data) {
            switch ($opt_data["type"]) {
            case "bool":
                $form[$opt_key] = [
                    '#type' => 'checkbox',
                    '#title' => $this->t($opt_data["title"]),
                    '#default_value' => $this->settings[$opt_key]
                ];
                break;
               }
        }
        return $form;
    }


    protected function addClasses(&$tag_element, $classes)
    {
        $existing_classes = $tag_element->getAttribute('class');
        while (strpos($existing_classes, "  ") !== false) {
            $existing_classes = str_replace("  ", " ", $existing_classes);
        }
        $definedClasses = explode(' ', $existing_classes);
        foreach ($classes as $className) {
            if (!in_array($className, $definedClasses)) {
                $tag_element->setAttribute(
                    'class',
                    $tag_element->getAttribute('class') . ' ' . $className
                );
            }
        }
    }

    /**
     * @TODO
     */
    protected function getDatatablesSettingsHardCoded()
    {
        return [
        "responsive" => true,
        "language" => [
                "sProcessing"=>     "Traitement en cours...",
                "sSearch"=>         "Rechercher&nbsp;:&nbsp;",
                "sLengthMenu"=>     "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo"=>           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty"=>      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered"=>   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix"=>    "",
                "sLoadingRecords"=> "Chargement en cours...",
                "sZeroRecords"=>    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable"=>     "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate"=> [
                            "sFirst"=>      "Premier",
                    "sPrevious"=>   "Pr&eacute;c&eacute;dent",
                    "sNext"=>       "Suivant",
                    "sLast"=>       "Dernier"
                ],
                "oAria"=> [
                            "sSortAscending"=>  "=> activer pour trier la colonne par ordre croissant",
                    "sSortDescending"=> "=> activer pour trier la colonne par ordre d&eacute;croissant"
                ],
                "select"=> [
                            "rows"=> [
                                "_"=> "%d lignes séléctionnées",
                            "0"=> "Aucune ligne séléctionnée",
                            "1"=> "1 ligne séléctionnée"
                            ]
                ]
        ]
        ];
    }

    protected function getDatatablesSettings()
    {
        $settings = $this->getDatatablesSettingsHardCoded();
        //\Drupal::logger("datatables_filter")->info("plugin settings<pre>".print_r($this->settings,TRUE)."</pre>");
        $opts = $this->getDatatablesOptions();
        foreach ($opts as $opt_key => $opt_data) {
            switch ($opt_data["type"]) {
            case "bool":
                $value = (bool)$this->settings[$opt_key];
                $settings[$opt_key] = $value;
                break;
            }
        }
        return $settings;
    }
    /**
     * {@inheritdoc}
     */
    public function process($text, $langcode)
    {
        $document = Html::load($text);
        $table_elements =  $document->getElementsByTagName("table");
        $classes_to_add = array('datatables-enabled');
        $datatablesSettings = $this->getDatatablesSettings();
        // \Drupal::logger("datatables_filter")->info("settings<pre>".print_r($datatablesSettings,TRUE)."</pre>");
        foreach ($table_elements as $tag_element) {
            /**
      * @var \DOMElement $tag_element
*/
            $this->addClasses($tag_element, $classes_to_add);
            $tag_element->setAttribute("data-datatables-filter-settings", json_encode($datatablesSettings));
        }

        $result = new FilterProcessResult(Html::serialize($document));
        $result->setAttachments(
            [
            'library' => ['datatables_filter/datatables_filter'],
            ]
        );

        return $result;
    }
}
